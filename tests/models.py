from flask_restful_swagger_3 import Schema 
from flask_restful import fields

class UserModel(Schema):
    type = 'object'
    properties = {
        'id': {
            'type': 'integer',
            'format': 'int64',
        },
        'name': {
            'type': 'string'
        }
    }
    required = ['name']


class SwaggerTestModel(Schema):
    """
    Test schema model.
    """
    type = 'object'
    properties = {
        'id': {
            'type': 'string'
        }
    }


class SchemaTestModel(Schema):
    """
    Test schema model.
    """
    type = 'object'
    properties = {
        'id': {
            'type': 'integer'
        },
        'name': {
            'type': 'string'
        }
    }
    required = ['id']

class SchemaTestModelFields(Schema):
    """
    Test schema model.
    """
    type = 'object'
    resource_fields = {
        'id': fields.Integer,
        'name': fields.String
    }
    required = ['id']